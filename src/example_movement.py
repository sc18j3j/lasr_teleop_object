#!/usr/bin/python

import rospy
from geometry_msgs.msg import Twist, Vector3

if __name__ == '__main__':
    rospy.init_node('lasr_teleop_movement')
    pub_0 = rospy.Publisher('/lasr_object_controller/person_0/cmd_vel', Twist, queue_size = 0)
    pub_1 = rospy.Publisher('/lasr_object_controller/person_1/cmd_vel', Twist, queue_size = 0)

    twist_0 = Twist(Vector3(0,-1,0), Vector3(0,0,-1))
    twist_1 = Twist(Vector3(0,-1,0), Vector3(0,0,1))

    pub_0.publish(twist_0)
    pub_1.publish(twist_1)